# What is IoT?
  The Internet of Things (IoT) refers to a system of interrelated, internet-connected objects that are able to collect and transfer data over a wireless network without human intervention.  
In simple term, we can say **connecting any electronic device to internet** like clock, power devices etc.

# What is Industry IoT?
 IIoT stands for the Industrial Internet of Things or Industrial IoT that initially mainly referred to an industrial framework whereby a large number of devices or machines are connected and synchronized through the use of software tools and third platform technologies in a machine-to-machine and Internet of Things context, later an Industry 4.0 or Industrial Internet context.  

In simple terms, we can say **making smart industries** by collecting and arranging the data using IoT.

 ![](/extras/IoT_vs_IIoT.jpg)

## Here comes the Industrial Revolution
![](/extras/revo.jpg)  
 **Today Still some industries are in 3.0**

## Industry 3.0
 ![](/extras/3.0arch.png)

## Industry 3.0 vs Industry 4.0
 ![vs](/extras/3.jpeg)

## Communication Protocols of Industry3.0
  Communication protocols are used for sending data to a central server inside the factory.
These protocols are used by sensors to send data to PLC's. These are called as fieldbus.
 
 ![comm](/extras/comm3.png)

## Industry 4.0
 Industry 4.0 is nothing but Industry 3.0 connected with internet called IoT(Internet of things)
They send you data when you are connected to Internet.  
 **EDGE(IoT Gateway)** which converts the ModBus data into cloud.

 ![](/extras/arch_4.0.png)

## What can you do with the data gathered from devices?

  Industry 4.0 is nothing but Industry 3.0 connected with internet. They send us data when we are connected to Internet.
 ![data](extras/data.png)

## Communication Protocols of Industry4.0
  These protocols are used for sending data to cloud for data analysis.

 ![](/extras/comm4.png)

## Problems with 4.0 upgrade
1. **Cost** - Industry 3.0 devices are very Expensive as a factory owner I dont want to switch to Industry 4.0 devices because they are Expensive.
2. **Downtime** - Changing Hardware means a big factory downtime, I dont want to shut down my factory to upgrade devices.
3. **Reliable** - I dont want to invest in devices which are unproven and unreliable.

## Solution
 Designing cheap devices without changes to the original device and then send data to the cloud using Industry 4.0 devices

## Challenges in conversion
1. Expensive Hardware
2. Lack of documentation
3. Properitary PLC protocols

## How to convert?
 We have a library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud  
 
 ![](/extras/roadmap.png)

## How it looks inside the IoT cloud ?

![](/extras/look.png)

# Sample Industry device
 **Conzerv EM1000/EM1200/EM1220 Power and Energy Meter datasheet**  
 
 ![](/extras/meter.png)

## Checking Interface and Protocols
 
 ![](/extras/pr.png)
 ![](/extras/in.png)

